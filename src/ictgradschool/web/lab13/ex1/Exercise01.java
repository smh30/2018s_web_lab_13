package ictgradschool.web.lab13.ex1;

import ictgradschool.web.Keyboard;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;
import java.util.Scanner;

public class Exercise01 {
    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        Properties dbProps = new Properties();
        String searchString = "";

        while (searchString.length() == 0) {
            System.out.println("Enter an article to search for: ");
            searchString = Keyboard.readInput();

            try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
                dbProps.load(fIn);
            } catch (IOException e) {
                e.printStackTrace();
            }

            // Set the database name to your database
            try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
                //System.out.println("Connection successful");
                //Anything that requires the use of the connection should be in here...

                try (PreparedStatement stmt = conn.prepareStatement(
                        "SELECT body FROM lab13_articles WHERE title LIKE ?")) {

                    searchString = "%" + searchString.trim() + "%";
                    stmt.setString(1, searchString);

                    try (ResultSet r = stmt.executeQuery()) {
                        while (r.next()) {
                            System.out.println(r.getString(1));
                        }
                    }

                }


            } catch (SQLException e) {
                e.printStackTrace();
            }
            System.out.println("Search complete\n");
            System.out.println("To quit press Q, otherwise enter: ");
            String k = Keyboard.readInput().trim();
            if (k.equalsIgnoreCase("q")){
                searchString = "q";
            } else {
                searchString = "";
            }
        }
    }
}
